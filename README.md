LWLab 7-way wire harness
========================

## Latest updates: 

### Thursday, March 20 2014

 - Created exploded assembly and assembly procedure
 - Adjusted the 4 K shield holder to make assembly easier
 - Created assembly process screenshots and video

### Wednesday, March 19 2014

 - Inserted shield blanks, mounting holes, mounting plates

### Thursday, March 6th 2014

- Updated the dimensions of the 50 K and 4 K shield holders to increase wire slack
- Changed the configuration of the 4 K holder from inverted to normal configuration (mounted to the outer wall of the 4 K shield)

### Tuesday, March 4th 2014

 - Merged the 7-way branch into the master branch.
 - Updated the geometry of the shield holders to match the new wire lengths.
 - Updated the 4 K shield holder so that it is inverted, so that the extra wire length can be accommodated
 - Changed the dimensions of the ISO200 flange tube to match HPD's confirmation of a 0.75'' reduction in tube length